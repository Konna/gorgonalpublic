#pragma once

#ifndef _EntityH
#define _EntityH

#include <io.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <list>

#include "HGE\include\hge.h"
#include "HGE\include\hgevector.h"
#include "HGE\include\hgeanim.h"
#include "HGE\include\hgerect.h"

#include "GoalManager.h"


using namespace std;

class Entity
{
protected:
	hgeVector startLocation;
	hgeVector velocity;
	hgeRect boundBox;
	hgeAnimation* baseSprite;
	float angle;
	float height;
	float width;
	int frames;
	hgeVector location;
	bool colliding;
	void notify(Entity& entity, Observer::Goal goal);
	void notify(Entity& entity, int distance);
	void notify(Entity& entity, Observer::Hit hit);
	void setTexture(HTEXTURE newTextureSheet) { baseSprite->SetTexture(newTextureSheet); };
	list<Observer*> observerList;
public:
	Entity(hgeVector setLocation, hgeVector setVelocity, HTEXTURE &setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth);
	Entity();
	~Entity(void);
	virtual void draw();
	virtual void update(float delta);
	hgeRect getBoundBox() const {return boundBox;};
	float getAngle() { return angle;};
	int RGBATOCOLOR(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	int RGBTOCOLOR(char r, char g, char b);
	void drawRect(int x, int y, int w, int h, int c,HTEXTURE tex=NULL);
	bool checkCollision(Entity* object);
	hgeVector getLocation(){ return location; };
	void setVelocity(hgeVector setVelocity){ velocity = setVelocity; };
	void setVelocity(float x, float y) { velocity = hgeVector(x,y); };
	void setLocation(hgeVector setLocation){ location = setLocation; };
	void setLocation(float x, float y){ location = hgeVector(x,y); };
	void setAngle(float setAngle) { angle = setAngle; };
	void setFrames(int setFrames){frames = setFrames; };
	static HGE* hge;
};
#endif

