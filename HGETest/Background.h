#pragma once

#ifndef _BackgroundH
#define _BackgroundH

#include "HGE\include\hgesprite.h"
#include "HGE\include\hgevector.h"
#include "HGE\include\hge.h"



class Wrapper;
class Background
{
public:
	//Variables
	static HGE *hge;
	hgeSprite *backgroundSpriteArray[18];
	hgeSprite *grass;
	hgeSprite *grass2;
	hgeSprite *grass3;
	hgeVector location;
	hgeVector nextLocation;
	hgeVector locationA;
	hgeVector locationB;
	hgeVector locationC;
	//Methods
	Background(HTEXTURE* setTexture, hgeVector setlocation, float setHeight, float setWidth);
	~Background(void);
	void reset();
	void draw();
	void update(Wrapper& wrapper);
private:
};
#endif
