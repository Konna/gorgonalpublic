#include "MenuState.h"
#include "Wrapper.h"

StartMenu MenuState::mainMenu;


///<summary>
/// Default Constructor.
///</summary>
MenuState::MenuState(void)
{
	mainMenu = StartMenu();
}

///<summary>
/// Constructor that takes in an array of textures.
///</summary>
MenuState::MenuState(HTEXTURE* startMenuTex, Wrapper* wrapper)
{
	mainMenu = StartMenu(startMenuTex, wrapper);
}


MenuState::~MenuState(void)
{
}

///<summary>
/// In this method we draw the appropriate menu
///</summary>
void MenuState::draw(Wrapper& wrapper)
{
	mainMenu.draw();
}


///<summary>
/// In this method we update the appropriate menu
///</summary>
void MenuState::update(Wrapper& wrapper)
{
	float delta = wrapper.hge->Timer_GetDelta();
	mainMenu.update(delta);
}