#include "BaseMenu.h"
#include "Wrapper.h"
///<summary>
/// Default Constructor creates the menu object.
///</summary>
BaseMenu::BaseMenu(void)
{
	menu = new hgeGUI();
}


BaseMenu::~BaseMenu(void)
{
}


///<summary>
/// In this method we draw the background and the buttons
///</summary>
void BaseMenu::draw()
{
	menu->Render();
}

///<summary>
/// In this method we update our buttons based on user input
///</summary>
void BaseMenu::update(float delta)
{

}

