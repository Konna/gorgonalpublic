#include "GameState.h"

DayState GameState::day;
MenuState GameState::menu;
ShopState GameState::shop;

///<summary>
/// Constructor that takes serveral arrays of textures for the various menuStates.
///</summary>
GameState::GameState(HTEXTURE* startMenuTex, HTEXTURE* shopMenuTex, Wrapper* wrapper)
{
	menu = MenuState(startMenuTex, wrapper);
	shop = ShopState(shopMenuTex, wrapper);
	day = DayState();
	previousState = &day;
	currentState = &day;
}


GameState::~GameState(void)
{
}

///<summary>
/// Sets the current state to the new state and and updates the previous state
///</summary>
void GameState::setState(WrapperState* state)
{
	// TODO: Set popable states so the system can't change to/from states that aren't allowed.
	previousState = currentState;
	currentState = state;
}

///<summary>
/// Resets the game state to the initial state of the game
///</summary>
void GameState::resetGame()
{
	currentState = &day;
}
