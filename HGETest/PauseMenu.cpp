#include "PauseMenu.h"

///<summary>
/// Default Constructor
///</summary>
PauseMenu::PauseMenu(void): BaseMenu()
{
}

///<summary>
/// Constructor taking in an arrary of textures
///</summary>
PauseMenu::PauseMenu(HTEXTURE* setTexture) : BaseMenu()
{
	//TODO: These are temporaray to avoid compile errors they need actual values from image files.
	int x = 0, y = 0, w = 0, h = 0, tx = 0, ty = 0;
	background =  new hgeSprite(setTexture[0],x,y,w,h);
	for (int i = 1; i <= 2; i++)
	{
		hgeGUIButton tempButton = hgeGUIButton(i, x, y, w, h, setTexture[i], tx, ty);
		menu->AddCtrl(&tempButton);
	}
}

PauseMenu::~PauseMenu(void) 
{

}

///<summary>
/// In this method we update our buttons based on user input
///</summary>
void PauseMenu::update(float delta)
{
	menu->Update(delta);
}