#include "Camera.h"
#include "PlayerState.h"

///<summary>
/// Default Constructor.
///</summary>
Camera::Camera(void)
{

	speed = 60;
	hge = hgeCreate(HGE_VERSION);
	wKeyFlag = false;
	sKeyFlag = false;
	aKeyFlag = false;
	dKeyFlag = false;
	viewPort = LogicRectangle(0,0,800,600);
	dy = viewPort.centreY;
	dx = viewPort.centreX;
}


Camera::~Camera(void)
{
	hge->Release();
}


///<summary>
/// Translate the viewport to the cameras location.
///</summary>
void Camera::draw(PlayerState* state)
{
	if (state->name() == "LaunchState")
	{
		hge->Gfx_SetTransform(viewPort.width,viewPort.height,viewPort.centreX,viewPort.centreY,0,1,1);
	}
	else
	{
		hge->Gfx_SetTransform(viewPort.width,viewPort.height,dx,dy,0,1,1);
	}
}

///<summary>
/// Update the cameras location. 
///</summary>
void Camera::update(float delta)
{
	viewPort.centreX = (800/2) - dx;
	viewPort.centreY = (600/2) - dy;
}

void Camera::reset()
{
	viewPort.centreX = 0;
	viewPort.centreY = 0;
	viewPort.height = 600;
	viewPort.width = 800;
	dx = 1;
	dy = 1;
}