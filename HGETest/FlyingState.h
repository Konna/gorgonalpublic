#pragma once
#ifndef _FlyingStateH
#define _FlyingStateH
#include "playerstate.h"
class FlyingState : public PlayerState
{
public:
	FlyingState(void);
	virtual ~FlyingState(void);
	virtual void draw(Gorgonal& player);
	virtual void update(Gorgonal& player);

protected:
	float gravity;
};
#endif

