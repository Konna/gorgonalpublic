#include "GlidingState.h"
#include "Gorgonal.h"


///<summary>
/// Default Constructor.
///</summary>
GlidingState::GlidingState(void)
{
	gravity = 9.8f; //TODO: gravity and other variables should be changed to provide a gliding feeling. ~Konna
	startedInState = false;
	stateName = "GlidingState";
}

///<summary>
/// Constructor that takes in the texture sheet for the gliding state.
///</summary>
GlidingState::GlidingState(HTEXTURE setGlidingSheet)
{
	glidingSheet = setGlidingSheet;
	startedInState = false;
	stateName = "GlidingState";
}

GlidingState::~GlidingState(void)
{
}

///<summary>
/// In this method we handle any tasks required on entry to the state.
///</summary>
void GlidingState::enter(Gorgonal& player)
{
	player.setTexture(glidingSheet);
}

///<summary>
/// In this method we draw the correct sprite for player gliding in the air.
///</summary>
void GlidingState::draw(Gorgonal& player)
{

}

///<summary>
/// In this method we update the player to glide for a time.
///</summary>
void GlidingState::update(Gorgonal& player)
{
	float delta = (float)player.hge->Timer_GetDelta();
	//TODO: Changes to make provide a gliding feeling. ~Konna
	//TODO: Add an exit clause for returning to flying state i.e. run out of durability or fuel. ~Konna 
	if (!player.completed)
	{
		player.velocity.y -= (gravity * (5 * delta));
		if(player.location.y > 0)
		{
			player.power = player.power * 0.75;
			if(player.power < 1)
			{
				player.power = 0;
			}
			player.velocity.y = (player.power) * player.scaleY;
			player.velocity.x = (player.power) * player.scaleX;
		}
		player.location.y += (-player.velocity.y * (5 * delta));
		player.location.x += (player.velocity.x * (5 * delta));
		if(player.location.y <= -3300) player.location.y = -3300;
		player.distance = player.location.x;
		if(player.power == 0)
		{
			player.completed = true;
			std::cout << player.consolePrefix << "Final distance: " << player.getDistance() << std::endl;
		}
	}
}