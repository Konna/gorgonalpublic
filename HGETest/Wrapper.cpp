#include "Wrapper.h"


HGE* Wrapper::hge = 0;
string Wrapper::consolePrefix = "[Wrapper]";
string Wrapper::resourceFolder = "../HGETest/Resources/";

///<summary>
/// Constructor.
///</summary>
Wrapper::Wrapper(void)
{
	hge = hgeCreate(HGE_VERSION);
}

///<summary>
/// Deconsturctor.
///</summary>
Wrapper::~Wrapper(void)
{

}

///<summary>
/// Add all the textures and audio here using addTexture("filepath") and addAudio("filepath").
///</summary>
void Wrapper::initAssets()
{
	addTexture("GorgonalLaunchFlight.png");
	addTexture("GorgonalWalkingSprites.png");
	//PICKUPS
	addTexture("Pickups/coin.png");//0
	addTexture("Pickups/fuel.png");//1
	//OBSTACLES
	//BOUNCY
	addTexture("Obstacles/Bouncy/bouncya.png");//0
	addTexture("Obstacles/Bouncy/bouncym.png");//1
	addTexture("Obstacles/Bouncy/bouncyr.png");//2
	addTexture("Obstacles/Bouncy/bouncyi.png");//3
	addTexture("Obstacles/Bouncy/bouncyo.png");//4
	addTexture("Obstacles/Bouncy/bouncyf.png");//5
	//SLOW
	addTexture("Obstacles/Slow/slowa.png");//6
	addTexture("Obstacles/Slow/slowm.png");//7
	addTexture("Obstacles/Slow/slowr.png");//8
	addTexture("Obstacles/Slow/slowi.png");//9
	addTexture("Obstacles/Slow/slowo.png");//10
	addTexture("Obstacles/Slow/slowf.png");//11
	//SPIKE
	addTexture("Obstacles/Spike/spikea.png");//12
	addTexture("Obstacles/Spike/spikem.png");//13
	addTexture("Obstacles/Spike/spiker.png");//14
	addTexture("Obstacles/Spike/spikei.png");//15
	addTexture("Obstacles/Spike/spikeo.png");//16
	addTexture("Obstacles/Spike/spikef.png");//17
	//LAUNCHERS
	addTexture("Cannon-barrel.png");
	addTexture("Cannon-Base.png");
	//BACKGROUNDS
	addTexture("Backgrounds/1-1.png");//0
	addTexture("Backgrounds/1-2.png");//1
	addTexture("Backgrounds/1-3.png");//2
	addTexture("Backgrounds/2-1.png");//3
	addTexture("Backgrounds/2-2.png");//4
	addTexture("Backgrounds/2-3.png");//5
	addTexture("Backgrounds/3-1.png");//6
	addTexture("Backgrounds/3-2.png");//7
	addTexture("Backgrounds/3-3.png");//8
	addTexture("Backgrounds/4-1.png");//9
	addTexture("Backgrounds/4-2.png");//10
	addTexture("Backgrounds/4-3.png");//11
	addTexture("Backgrounds/5-1.png");//12
	addTexture("Backgrounds/5-2.png");//13
	addTexture("Backgrounds/5-3.png");//14
	addTexture("Backgrounds/6-1.png");//15
	addTexture("Backgrounds/6-2.png");//16
	addTexture("Backgrounds/6-3.png");//17
	addTexture("Backgrounds/grass.png");//18
	//MENU
	addTexture("Menus/startBackground.png");//1
	addTexture("Buttons/startSheet.png");//2
	addTexture("cursor.png");//0
	//SHOP
	addTexture("Menus/Shop.png");
	addTexture("Buttons/shopSheet.png");
	addTexture("Buttons/available.png");
	//AUDIO
	addAudio("menu.wav");
}

///<summary>
/// Initialise all things here.
///</summary>
void Wrapper::initialise()
{
	list<HTEXTURE>::iterator textureItr;
	list<HEFFECT>::iterator soundItr;
	textureItr = textures.begin();
	soundItr = sounds.begin();
	hgeVector launcherLocation = hgeVector(0,0);
	camera = new Camera();
	shopManager = new ShopManager();
	inventory = new Inventory();
	HTEXTURE gorgonalSprites[2];
	gorgonalSprites[0] = *textureItr++;
	gorgonalSprites[1] = *textureItr++;
	testBall = new Gorgonal(hgeVector(0,0),launcherLocation, inventory, *soundItr++, gorgonalSprites[0], gorgonalSprites[0], gorgonalSprites[1], 1, hgeVector(0,0), 64.0f, 64.0f);
	HTEXTURE itemArray[2];
	for(int i = 0; i < 2; i++)
	{
		itemArray[i] = *textureItr++;
	}
	HTEXTURE obstacleArray[18];
	for(int i = 0;i < 18; i++)
	{
		obstacleArray[i] = *textureItr++;
	}
	worldControl = new WorldHandling(itemArray,obstacleArray, hgeVector(0,0), inventory);
	HTEXTURE launcherArray[2];
	launcherArray[0] = *textureItr++;
	launcherArray[1] = *textureItr++;
	testLauncher = new Launcher(hgeVector(0,0),launcherLocation,launcherArray,1,hgeVector(0,0),128.0f,128.0f);
	HTEXTURE backgroundArray[19];
	for(int i = 0;i < 19; i++)
	{
		backgroundArray[i] = *textureItr++;
	}
	testBackground = new Background(backgroundArray, hgeVector(-800,-600),800.0f,600.0f);
	HTEXTURE startMenuSheet[3];
	for(int i = 0;i < 2; i++)
	{
		startMenuSheet[i] = *textureItr++;
	}
	startMenuSheet[2] = *textureItr;
	HTEXTURE shopMenuSheet[4];
	for(int i = 0; i < 4; i++)
	{
		shopMenuSheet[i] = *textureItr++;
	}
	stateManager = new GameState(startMenuSheet, shopMenuSheet, this);
	state = &GameState::menu;
}

///<summary>
/// Insert a pointer reference to the draw member of objects you create
///</summary>
void Wrapper::draw()
{
	state->draw(*this);
}

///<summary>
/// More pointer references to the update memeber of objects created, also game logic and states do stuff here.
///</summary>
void Wrapper::update(float delta)
{
	state->update(*this);
}