#include "WorldHandling.h"

AncientEra WorldHandling::ancientEra;
IndustrialEra WorldHandling::industrialEra;
MedievalEra WorldHandling::medievalEra;
RenaissanceEra WorldHandling::renaissanceEra;
FutureEra WorldHandling::futureEra;
MordernEra WorldHandling::mordernEra;


//TODO: Create draw, cleanup for obstacle. add it to update and shit
//IDEA: Add air resistance, crouch goes faster down, widen to slow down fall and with wings use momentum
//track highest point reached since last bounce if normal physics doesnt work out

///<summary>
/// Default constructor.
///</summary>
WorldHandling::WorldHandling(HTEXTURE* setItemTexture,HTEXTURE* setObstacleTexture, hgeVector playerPos, Inventory* inventory)
{
	for(int i = 0;i < 2;i++)
	{
		itemTexture[i] = setItemTexture[i];
	}
	for(int i = 0;i < 18;i++)
	{
		obstacleTexture[i] = setObstacleTexture[i];
	}
	inventoryObserver = inventory;
	itemGenBorder = LogicRectangle(0,0,2000,2000);	
	itemList = list<Item>();
	itemLimit = 2;
	obstacleList = list<Obstacle>();
	obstacleLimit = 5;
	obstacleLocation.x = 0;
	obstacleLocation.y = 0;
	itemLocation.x = 0;
	itemLocation.y = 0;
	ancientEra = AncientEra();
	futureEra = FutureEra();
	mordernEra = MordernEra();
	medievalEra = MedievalEra();
	industrialEra = IndustrialEra();
	state = &ancientEra;
	generateNewItems(itemTexture, playerPos);
	generateNewObstacles(playerPos);

}

WorldHandling::~WorldHandling(void)
{ 
}

///<summary>
/// Updating the worldhandling removes items, scenary and enemies out of range and generates new ones.
///</summary>
void WorldHandling::update(float delta, hgeVector newLocation)
{
	itemGenBorder.centreX = newLocation.x;
	itemGenBorder.centreY = newLocation.y;
	cleanUpItems(newLocation);
	while (itemList.size() < itemLimit)
	{
		generateNewItems(itemTexture, newLocation);
	}
	while (obstacleList.size() < obstacleLimit)
	{
		generateNewObstacles(newLocation);
	}
}

///<summary>
/// This method draws all the needed background scenary, items and enemies on the screen
///</summary>
void WorldHandling::draw()
{
	list<Item>::iterator itr;
	list<Obstacle>::iterator itr2;
	for(itr = itemList.begin(); itr != itemList.end(); ++itr)
	{
		(*itr).draw();
	}
	for(itr2 = obstacleList.begin(); itr2 != obstacleList.end(); ++itr2)
	{
		(*itr2).draw();
	}
}

///<summary>
/// Deletion of the items outside of the generation.
///</summary>
void WorldHandling::cleanUpItems(hgeVector playerPos)
{
	bool markedForDeletion = false;
	list<Item>::iterator itr;
	list<Item>::iterator tempItr;
	list<Obstacle>::iterator itr2;
	list<Obstacle>::iterator tempItr2;
	for(itr = itemList.begin(); itr != itemList.end(); ++itr)
	{
		if(markedForDeletion)
		{
			cout << "deleting once" << endl;
			itemList.erase(tempItr);
			markedForDeletion = false;
		}
		if (((itr)->getLocation().x) <= (playerPos.x - 410) )
		{
			tempItr = itr;
			markedForDeletion = true;
		}
	}
	if (markedForDeletion)
	{
		cout << "Loop ended." << endl;
		itemList.erase(tempItr);
		markedForDeletion = false;
	}
	for(itr2 = obstacleList.begin(); itr2 != obstacleList.end(); ++itr2)
	{
		if(markedForDeletion)
		{
			cout << "deleting obstacle once" << endl;
			obstacleList.erase(tempItr2);
			markedForDeletion = false;
		}
		if (((itr2)->getLocation().x) <= (playerPos.x - 410) )
		{
			tempItr2 = itr2;
			markedForDeletion = true;
		}
	}
	if (markedForDeletion)
	{
		cout << "Loop ended." << endl;
		obstacleList.erase(tempItr2);
		markedForDeletion = false;
	}
}

void WorldHandling::reset()
{
	itemLocation.x = 0;
	itemLocation.y = 0;
	itemList.clear();
	obstacleLocation.x = 0;
	obstacleLocation.y = 0;
	obstacleList.clear();
}

///<summary>
/// Generate new items within the border and outside of viewport.
///</summary>
void WorldHandling::generateNewItems(HTEXTURE* itemText, hgeVector playerPos)
{
	itemLocation.x += 400;
	itemLocation.y = playerPos.y - ((rand() % 100) + 50);
	if((rand() % 10) == 0)
	{
		Item part = Item(hgeVector(0,0),itemLocation,inventoryObserver,itemText[1],1,hgeVector(0,0),32.0f,32.0f,part.part);
		itemList.push_back(part);
		cout << "part added" << std::endl;
	}
	else
	{
		Item coin = Item(hgeVector(0,0),itemLocation,inventoryObserver,itemText[0],1,hgeVector(0,0),32.0f,32.0f,coin.coin);
		itemList.push_back(coin);
		cout << "coin added" << std::endl;
	}
}

///<summary>
/// Delegate to state.
///</summary>
void WorldHandling::generateNewObstacles(hgeVector playerPos)
{
	state->generateNewObstacles(*this,playerPos);
}

bool WorldHandling::checkBorders(hgeRect a, hgeRect b)
{
	return a.Intersect(&b);
}

void WorldHandling::setState()
{
	if(state == &ancientEra)
	{
		state = &medievalEra;
	}
	else if(state == &medievalEra)
	{
		state = &renaissanceEra;
	}
	else if(state == &renaissanceEra)
	{
		state = &industrialEra;
	}
	else if(state == &industrialEra)
	{
		state = &mordernEra;//MODERN woops
	}
	else if(state == &mordernEra)
	{
		state = &futureEra;
	}
}

