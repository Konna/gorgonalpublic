#pragma once
#ifndef _WrapperH
#define _WrapperH
#include "HGE\include\hge.h"
#include <list>
#include <io.h>
#include <stdio.h>
#include <string>
#include <iostream>

#include "Gorgonal.h"
#include "Launcher.h"
#include "Camera.h"
#include "Background.h"
#include "WorldHandling.h"
#include "Item.h"
#include "Inventory.h"
#include "LogicRectangle.h"
#include "GameState.h"
#include "ShopManager.h"

using namespace std;
class Wrapper 
{
	friend class DayState;
public:
	Wrapper(void);
	~Wrapper(void);
	void initialise();
	void initAssets();
	virtual void update(float delta);
	virtual void draw();
	list<string> textureFilePath;
	list<string> audioFilePath; 
	list<string> musicFilePath;
	list<HTEXTURE> textures;
	list<HEFFECT> sounds;
	list<HMUSIC> music;

	static string resourceFolder; 
	void addTexture(string stringToFile) { textureFilePath.push_back(resourceFolder + stringToFile); } ;
	void addAudio(string stringToFile) { audioFilePath.push_back(resourceFolder + stringToFile); };
	void addMusic(string stringToFile) { musicFilePath.push_back(resourceFolder + stringToFile); };
	Gorgonal* testBall;
	Launcher* testLauncher;
	Camera* camera;
	Background* testBackground;
	Inventory* inventory;
	WorldHandling* worldControl;
	ShopManager* shopManager;
	static HGE* hge;
	static string consolePrefix; 
	void onNotify(WrapperState* setState) { state = setState;};

private:
	GameState* stateManager;
	WrapperState* state;
};
#endif