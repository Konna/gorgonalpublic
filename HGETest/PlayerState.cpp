#include "PlayerState.h"
#include "Gorgonal.h"


///<summary>
/// Default Constructor.
///</summary>
PlayerState::PlayerState(void)
{
}


PlayerState::~PlayerState(void)
{
}

///<summary>
/// In this method we perform tasks that are required on entering the state.
///</summary>
void PlayerState::enter(Gorgonal& player)
{

}

///<summary>
/// In this method we draw dependent on state.
///</summary>
void PlayerState::draw(Gorgonal& player)
{

}

///<summary>
/// In this method we update dependent on state.
///</summary>
void PlayerState::update(Gorgonal& player)
{

}