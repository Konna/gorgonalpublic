#pragma once
#ifndef _StartMenuH
#define _StartMenuH
#include "basemenu.h"

class StartMenu : public BaseMenu
{
public:
	StartMenu(void);
	StartMenu(HTEXTURE* setTextures, Wrapper* wrapper);
	~StartMenu(void);
	virtual void update(float delta);
	virtual void draw();
private:
	string consolePrefix;
	hgeGUIButton* start;
	hgeGUIButton* credits;
	hgeGUIButton* exit;
};
#endif
