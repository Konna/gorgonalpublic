#include "Inventory.h"
#include "Entity.h"

Inventory::Inventory(void)
{
	observerList = list<Observer*>();
	coins = 0;
	distanceToCoin = 50;
}


Inventory::~Inventory(void)
{
}

///<summary>
/// This method checks if the player has obtained an upgrade and returns true if they have.
///</summayr>
bool Inventory::contains(string checkUpgrade)
{
	list<string>::iterator itr;
	for (itr = upgradeList.begin(); itr != upgradeList.end(); ++itr)
	{
		if ((*itr).compare(checkUpgrade) == true)
		{
			return true;
		}
	}
	return false;
}

void Inventory::setCoins(int setCoin)
{
	coins = setCoin;
}

///<summary>
/// Lol as if this method actually does anything.
///</summary>
void Inventory::update()
{

}

///<summary>
///
///</summary>
void Inventory::onNotify(Entity& entity, int distance)
{
	int coinGains = distance / distanceToCoin;
	coins += coinGains;
	notify(*this, coins);
}

///<summary>
///
///</summary>
void Inventory::onNotify(Entity& entity, Observer::Hit hit)
{
	switch(hit)
	{
	case COIN:
		{
			coins += 1;
			break;
		}
	case FUEL:
		{
			fuel += 1;
			break;
		}
	default:
		{
			break;
		}
	}
}

///<summary>
/// In this method we update lists based upon a purchase made.
///</summary>
void Inventory::onNotify(Entity& entity, Observer::Purchase purchase)
{
	list<Observer::Purchase>::iterator itr = find(unlockedList.begin(), unlockedList.end(), purchase);
	unlockedList.erase(itr);
	purchasedList.emplace_back(purchase);
	distanceToCoin = 1;
	//TODO: Here we should change things so that when specific purchases are made, i.e. Power one power two is moved to the unlocked list. ~Konna
}

///<summary>
/// In this method we notify any of the observers watching inventory of the current coins the player has.
///</summary>
void Inventory::notify(Inventory& inventory, int currentCoins)
{
	list<Observer*>::iterator itr;
	for (itr = observerList.begin(); itr != observerList.end(); ++itr)
	{
		(*itr)->onNotify(inventory, currentCoins);
	}
}

///<summary>
/// In this method we check if the unlocked list contains a certain purchase.
///</summary>
bool Inventory::checkUnlocked(Observer::Purchase checkThis)
{
	list<Observer::Purchase>::iterator itr = find(unlockedList.begin(), unlockedList.end(), checkThis);
	if (itr != unlockedList.end()) return true;
	return false;
}