#include "LaunchingState.h"
#include "Gorgonal.h"

///<summary>
/// Default Constructor.
///</summary>
LaunchingState::LaunchingState(void)
{
	startedInState = false;
	stateName = "LaunchState";
}

///<summary>
/// Constructor that takes in a texture for the player in its launch state.
///</summary>
LaunchingState::LaunchingState(HTEXTURE &setLaunchingSheet)
{
	launchingSheet = setLaunchingSheet;
	startedInState = false;
	stateName = "LaunchState";
}

LaunchingState::~LaunchingState(void)
{
}


///<summary>
/// In this method we perform any tasks required on entry to the state.
///</summary>
void LaunchingState::enter(Gorgonal& player)
{

	player.setTexture(launchingSheet);
	player.launched = false;
	player.completed = false;
	player.location.x = player.startLocation.x;
	player.location.y = player.startLocation.y;
	player.velocity.x = 0;
	player.velocity.y = 0;
	player.dy = 0;
	player.power = 0;
	player.angle = 45;
	player.dx = 0;
	startedInState = true;
	cout << "entered launchingState" << endl;
}

///<summary>
/// In this method we draw the player in its launch state.
///</summary>
void LaunchingState::draw(Gorgonal& player)
{
	float drawAngle = player.angle - 90;
	player.baseSprite->RenderEx(player.location.x,player.location.y,-(drawAngle * (M_PI/180)));
}

///<summary>
/// In this method we update the player in its launch state.
///</summary>
void LaunchingState::update(Gorgonal& player)
{
	if(startedInState == false)
	{
		enter(player);
	}
	if (player.launched)
	{
		startedInState = false;
		player.state = &player.flying;
	}
}