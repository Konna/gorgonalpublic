#include "Launcher.h"

///<summary>
/// Constructor for the creation of the Launcher.
///</summary>
Launcher::Launcher(hgeVector velocity, hgeVector location, HTEXTURE* setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth)
	: Entity(location, velocity, setTexture[0], setFrames, textureLocation, setHeight, setWidth)
{
	upKeyFlag = false;
	downKeyFlag = false;
	spaceKeyFlag = false;
	increase = true;
	consolePrefix = "[Launcher]";
	power = 10;
	angle = 45;
	baseSprite->SetHotSpot(70,64);
	launchPad = new hgeSprite(setTexture[1],location.x,location.y,128,128);
	launchPad->SetHotSpot(60,70);
}


Launcher::~Launcher(void)
{
	
}

///<summary>
/// Update controls the power via user input, later versions will see the power scale outside of the user input adding the challenge of timing.
///</summary>
void Launcher::update(float delta)
{
	if(upKeyFlag)
	{
		if ((power < 100.0)&&(power >= 0)) power += 0.1;
		if (power > 100) power = 100;
		std::cout << consolePrefix << " Launcher power is: " << getPower() << std::endl;
	}
	else if(downKeyFlag)
	{
		if ((power > 0.0)&&(power <= 100)) power -= 0.1;
		if (power < 0) power = 0;
		std::cout << consolePrefix << " Launcher power is: " << getPower() << std::endl;
	}
	if(leftKeyFlag)
	{
		if ((angle >= 0.0)&&(angle < 89)) angle += 0.5;
		if (angle < 0) angle = 0;
		std::cout << consolePrefix << " Launcher angle is: " << getAngle() << std::endl;
	}
	else if(rightKeyFlag)
	{
		if ((angle > 0)&&(angle <= 89)) angle -= 0.5;
		if (angle < 0) angle = 0;
		std::cout << consolePrefix << " Launcher angle is: " << getAngle() << std::endl;
	}
	if(spaceKeyFlag)
	{
		std::cout << consolePrefix << " Launcher power is: " << getPower() << std::endl;
	}
	if(increase)
	{
		power += 1;
		if(power >= 100)
		{
			increase = false;
		}
	}
	else
	{
		power -= 1;
		if(power <= 10)
		{
			increase = true;
		}
	}
}

///<summary>
/// Draw method should draw the paused animation of the launcher at it's angle and then play the animation once the user has fired the cannon.
///</summary>
void Launcher::draw()
{
	baseSprite->RenderEx(location.x,(location.y - 60),-(angle * (M_PI/180)));
	launchPad->Render(location.x,(location.y - 60));
}

void Launcher::reset()
{
	power = 0.0;
	angle = 45.0;
}