#pragma once
#include "erastate.h"
class RenaissanceEra :
	public EraState
{
public:
	RenaissanceEra(void);
	~RenaissanceEra(void);
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos);
};

