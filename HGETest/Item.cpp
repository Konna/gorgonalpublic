#include "Item.h"

///<summary>
/// Base class for the pick up items in the game.
///</summary>
Item::Item(hgeVector velocity, hgeVector location, Inventory* inventoryObserver, HTEXTURE &setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth, type setType) 
	: Entity(location, velocity, setTexture, setFrames, textureLocation, setHeight, setWidth)
{
	itemType = setType;
	consolePrefix = "[Item] " + itemType;
	//type((-0 + rand()  % ( 1 - -0 + 1)));
	setTextureLocation();
	observerList = list<Observer*>();
	observerList.push_back(inventoryObserver);
	angle = 0;

}

Item::Item()
{
	itemType = type::part;
	consolePrefix = "[Item] Part";
}

Item::~Item(void)
{
	std::cout << consolePrefix << "cleaning item" << std::endl;
}

///<summary>
/// This method should set the texture to the appropriate one from the sprite sheet.
///</summary>
void Item::setTextureLocation()
{
	baseSprite->SetTextureRect(0,0,32,32);
}

///<summary>
/// Temporary draw method for items, the final method will draw items with the appropriate textures.
///</summary>
void Item::draw()
{
	if(!colliding)
	{
		baseSprite->Render(location.x,location.y);
	}
}

void Item::collision()
{
	if(colliding)
	{
		switch(itemType)
		{
		case coin:
			{
				notify(*this, Observer::COIN);
			}
			break;
		case part:
			{
				notify(*this,Observer::FUEL);
			}
			break;
		}
	}
}