#include "Background.h"
#include "Wrapper.h"

HGE* Background::hge = 0;

Background::Background(HTEXTURE* setTexture, hgeVector setLocation, float setHeight, float setWidth)
{
	hge = hgeCreate(HGE_VERSION);
	location = setLocation;
	for(int i= 0;i < 18;i++)
	{
		backgroundSpriteArray[i] = new hgeSprite(setTexture[i], location.x, location.y, setHeight, setWidth);
	}
	grass = new hgeSprite(setTexture[18],location.x,location.y,800,300);
	grass2 = new hgeSprite(setTexture[18],location.x,location.y,800,300);
	grass3 = new hgeSprite(setTexture[18],location.x,location.y,800,300);
	locationA = location;
	locationB.x = location.x + 800;
	locationB.y = location.y;
	locationC.x = location.x + 1600;
	locationC.y = location.y;
}


Background::~Background(void)
{
	for(int i= 0;i < 18;i++)
	{
		if(backgroundSpriteArray[i] != NULL)
		{
			delete backgroundSpriteArray[i];
		}
	}
	
	hge->Release();
}

void Background::draw()
{
	int j = 0;
	for(int i = 0;i < 18;i += 3)
	{
		backgroundSpriteArray[i]->Render(locationA.x,locationA.y - (j * 600));
		backgroundSpriteArray[i + 1]->Render(locationB.x,locationB.y - (j * 600));
		backgroundSpriteArray[i + 2]->Render(locationC.x,locationC.y - (j * 600));
		j++;
	}
	grass->Render(locationA.x,0);
	grass2->Render(locationB.x,0);
	grass3->Render(locationC.x,0);
}

void Background::reset()
{
	locationA.x = location.x;
	locationB.x = location.x + 800;
	locationC.x = location.x + 1600;
}

void Background::update(Wrapper& wrapper)
{
	if(wrapper.testBall->getLocation().x > (locationA.x + 1600))
	{
		locationA.x += 2400;
	}
	if(wrapper.testBall->getLocation().x > (locationB.x + 1600))
	{
		locationB.x += 2400;
	}
	if(wrapper.testBall->getLocation().x > (locationC.x + 1600))
	{
		locationC.x += 2400;
	}
}