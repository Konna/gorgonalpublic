#include "ShopMenu.h"
#include "Wrapper.h"
///<summary>
/// Default Constructor.
///</summary>
ShopMenu::ShopMenu(void)
{
}

///<summary>
/// Constructor taking in an arrary of textures.
///</summary>
ShopMenu::ShopMenu(HTEXTURE* setTextures, Wrapper* setWrapper) : BaseMenu()
{
	wrapper = setWrapper;
	//TODO: These are temporaray to avoid compile errors they need actual values from image files.
	int x = 0, y = 0, w = 800, h = 600, tx = 0, ty = 0;
	menu->SetCursor(new hgeSprite(setTextures[0],0,0,32,32));
	background = new hgeSprite(setTextures[1],x,y,w,h);
	menuFont = new hgeFont("menuFont.fnt");
	era = new hgeGUIButton(1,520,115,200,50,setTextures[2],0,0);
	exit = new hgeGUIButton(7,515,420,75,50,setTextures[2],0,50);
	available = new hgeSprite(setTextures[3],0,0,64,64);
	menu->AddCtrl(era);
	menu->AddCtrl(exit);
	menu->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
	menu->SetFocus(1);
	menu->Enter();
}

ShopMenu::~ShopMenu(void)
{
}

///<summary>
/// In this method we update the menu based on the user input.
///</summary>
void ShopMenu::update(float delta)
{
	menu->Update(delta);
	if(era->GetState())
	{
		if(wrapper->inventory->getCoins() >= wrapper->shopManager->getEraPrice())
		{
			wrapper->inventory->setCoins(wrapper->shopManager->purchase(wrapper->shopManager->getEraPrice(),wrapper->inventory->getCoins()));
			wrapper->shopManager->setEraPrice();
			era->SetState(false);
			wrapper->worldControl->setState();
			wrapper->onNotify(&GameState::day);
		}
	}
	if (exit->GetState())
	{
		exit->SetState(false);
		wrapper->onNotify(&GameState::day);
	}
}

void ShopMenu::draw()
{
	background->Render(0,0);
	if(wrapper->inventory->getCoins() >= wrapper->shopManager->getEraPrice())
	{
		available->Render(730,110);
	}
	BaseMenu::draw();
}