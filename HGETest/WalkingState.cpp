#include "WalkingState.h"
#include "Gorgonal.h"

///<summary>
/// Default Constructor.
///</summary>
WalkingState::WalkingState(void)
{
	startedInState = false;
	stateName = "WalkingState";
	frame = 0;
	frameTime = 0;
}

///<summary>
/// Constructor that takes in our spritesheet for walking.
///</summary>
WalkingState::WalkingState(HTEXTURE &setWalkingSheet)
{
	startedInState = false;
	stateName = "WalkingState";
	frame = 0;
	frameTime = 0;
	walkingSheet = setWalkingSheet;
}

WalkingState::~WalkingState(void)
{
}

///<summary>
/// In this method we perform any tasks that we need on entry to the state i.e. change textures.
///</summary>
void WalkingState::enter(Gorgonal& player)
{
	player.setTexture(walkingSheet);
	player.baseSprite->SetFrames(3);
	frame = 0;
	frameTime = 0;
	lastFrameTime = 0;
	stateTime = 0;
	startTime = (float)player.hge->Timer_GetTime();
	startedInState = true;
	cout << "entered WalkingState" << endl;
}

///<summary>
/// In this method we perform any tasks that we need on exiting the state i.e. reseting startedInState bool
///</summary>
void WalkingState::exit()
{
	startedInState = false;
	frame = 0;
	frameTime = 0;
	stateTime = 0;
	startTime = 0;

}

///<summary>
/// In this method we draw the appropriate frame for the animation.
///</summary>
void WalkingState::draw(Gorgonal& player)
{
	if (!startedInState)
	{
		enter(player);
	}
	player.baseSprite->Render(player.location.x,player.location.y);
}

///<summary>
/// In this method we update the frame number, account for the player speaking and change players state when appropriate.
///</summary>
void WalkingState::update(Gorgonal& player)
{
	if (!startedInState)
	{
		enter(player);
	}
	frameTime = (float)player.hge->Timer_GetTime();
	if ((frameTime - lastFrameTime) > 0.1)
	{
		player.baseSprite->SetFrame(++frame);
		player.location.x -= 2.5;
		lastFrameTime = (float)player.hge->Timer_GetTime(); 
	}
	stateTime = (float)player.hge->Timer_GetTime() - startTime;
	if (stateTime > 5.0)
	{
		player.state = &player.launching;
		exit();
	}
}