#pragma once
#ifndef _ObserverH
#define _ObserverH
class Entity;
class Inventory;
class GuiButton;
class WrapperState;
class Observer
{
	//friend class Gorgonal; //Not sure why this is a friend class? ~Konna
public:
	enum Goal {MAIN_GOAL, OBJECTS_30_HIT, OBJECTS_60_HIT, OBJECTS_100_HIT, BOUNCY_50_HIT, BOUNCY_100_HIT,
		SLOW_50_HIT, SLOW_100_HIT, SPIKES_50_HIT, SPIKES_100_HIT, TRAVEL_10K, TRAVEL_20K, TRAVEL_50K, TRAVEL_100K, SCORE_UNDER_50};
	enum Hit {COIN, FUEL, OBJECT};
	enum Purchase {POWER_ONE, POWER_TWO}; // Assign purchases a gold value.
	Observer(void);
	virtual ~Observer(void);
	virtual void onNotify(Entity& entity, Observer::Goal goal);
	virtual void onNotify(Entity& entity, int distance);
	virtual void onNotify(Entity& entity, Observer::Hit hit);
	virtual void onNotify(Inventory& inventory, int currentCoins);
};
#endif
