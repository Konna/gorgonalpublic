#include "ShopState.h"
#include "Wrapper.h"

ShopMenu ShopState::shopMenu;


///<summary>
/// Deafult Constructor.
///</summary>
ShopState::ShopState(void)
{
	shopMenu = ShopMenu();
}

///<summary>
/// Construtor that takes in an array of textures.
///</summary>
ShopState::ShopState(HTEXTURE* setTexture, Wrapper* setWrapper)
{
	shopMenu = ShopMenu(setTexture, setWrapper);
}

ShopState::~ShopState(void)
{
}

///<summary>
/// In this method we draw the shop menu on the screen and any other details that are needed.
///</summary>
void ShopState::draw(Wrapper& wrapper)
{
	wrapper.camera->draw(&Gorgonal::launching);
	shopMenu.draw();
}

///<summary>
/// In this method we update the shop menu and any other existing values.
///</summary>
void ShopState::update(Wrapper& wrapper)
{
	wrapper.camera->reset();
	float delta = wrapper.hge->Timer_GetDelta();
	shopMenu.update(delta);
}