#pragma once
#ifndef _AncientEraH
#define _AncientEraH


#include "erastate.h"

class AncientEra : public EraState
{
public:
	AncientEra(void);
	~AncientEra(void);
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos); 
};
#endif

