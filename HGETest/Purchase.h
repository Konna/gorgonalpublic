#pragma once
#ifndef _PurchaseH
#define _PurchaseH

#include <string>

using namespace std;

class Purchase
{
public:
	Purchase(void);
	Purchase(string setType, int setCost);
	~Purchase(void);
	int getCost(){ return cost; };
	string getType(){ return type; };
private:
	int cost;
	string type;
};
#endif

