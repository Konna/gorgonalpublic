#pragma once
#ifndef _WrapperStateH
#define _WrapperStateH
#include <string>
#include "HGE\include\hge.h"
using namespace std;
class Wrapper;
class WrapperState
{
public:
	WrapperState();
	virtual ~WrapperState(void);
	virtual void draw(Wrapper& wrapper);
	virtual void update(Wrapper& wrapper);
	virtual string name() { return "WrapperState"; };
	HGE* hge;
};
#endif
