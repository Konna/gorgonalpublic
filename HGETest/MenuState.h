#pragma once
#ifndef _MenuStateH
#define _MenuStateH
#include "WrapperState.h"
#include "StartMenu.h"

class MenuState : public WrapperState
{
public:
	static StartMenu mainMenu;
	MenuState(void);
	MenuState(HTEXTURE* startMenuTex, Wrapper* wrapper);
	virtual ~MenuState(void);
	virtual void draw(Wrapper& wrapper);
	virtual void update(Wrapper& wrapper);
	virtual string name() { return "MenuState"; };
};
#endif

