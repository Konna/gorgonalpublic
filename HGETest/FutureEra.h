#pragma once
#include "erastate.h"
class FutureEra :
	public EraState
{
public:
	FutureEra(void);
	~FutureEra(void);
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos);
};

