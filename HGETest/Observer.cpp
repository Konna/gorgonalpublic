#include "Observer.h"
#include "Entity.h"
#include "Inventory.h"
#include "WrapperState.h"
///<summary>
/// Default Constructor.
///</summary>
Observer::Observer(void)
{
}


Observer::~Observer(void)
{
}


///<summary>
/// Actions to take on an entity reaching a certain goal.
///</summary>
void Observer::onNotify(Entity& entity, Observer::Goal goal)
{

}

///<summary>
/// Actions to take on an entity hitting a certain distance.
///</summary>
void Observer::onNotify(Entity& entity, int distance)
{

}

///<summary>
/// Actions to take on an entity hitting a certain object.
///</summary>
void Observer::onNotify(Entity& entity, Observer::Hit hit)
{

}

///<summary>
/// Actions to take when notified about an inventory's coins.
///</summary>
void Observer::onNotify(Inventory& inventory, int currentCoins)
{

}
