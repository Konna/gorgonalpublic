#pragma once

#include "entity.h"

using namespace std; 

class Obstacle : public Entity
{
public:
	enum type {bouncy, slow, spike};
	Obstacle(hgeVector velocity, hgeVector location, HTEXTURE &setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth, type setType);
	Obstacle();
	~Obstacle(void);
	type getType() { return obstacleType;};
	bool getColliding(){ return colliding;};
	virtual void draw();
	float collision(float power);
private:
	void setTextureLocation();
	hgeVector texLocation;
	type obstacleType;
	string consolePrefix;
};

