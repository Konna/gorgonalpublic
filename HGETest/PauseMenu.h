#pragma once
#ifndef _PauseMenuH
#define _PauseMenuH
#include "basemenu.h"
class PauseMenu :
	public BaseMenu
{
public:
	PauseMenu(void);
	PauseMenu(HTEXTURE* setTexture);
	~PauseMenu(void);
	virtual void update(float delta);
};
#endif

