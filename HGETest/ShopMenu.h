#pragma once
#ifndef _ShopMenuH
#define _ShopMenuH
#include "BaseMenu.h"
class ShopMenu : public BaseMenu
{
	friend class ShopManager;
public:
	ShopMenu(void);
	ShopMenu(HTEXTURE* setTexture, Wrapper* wrapper);
	~ShopMenu(void);
	virtual void update(float delta);
	void draw();
protected:
	Wrapper* wrapper;
private:
	hgeGUIText* coins;
	hgeGUIButton* era;
	hgeSprite* available;
	hgeGUIButton* exit;
};
#endif

