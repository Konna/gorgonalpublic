#include "Gorgonal.h"

FlyingState Gorgonal::flying;
GlidingState Gorgonal::gliding;
WalkingState Gorgonal::walking;
LaunchingState Gorgonal::launching;

//TODO: Whole bunch of refactoring. ~Konna. LOL as if.

///<summary>
/// Constructor passing in parameters to Entity base class.
///</summary>
Gorgonal::Gorgonal(hgeVector velocity, hgeVector location, Inventory* inventoryObserver, HEFFECT &setSound, HTEXTURE &setLaunchTexture, HTEXTURE &setFlightTexture, HTEXTURE &setWalkTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth)
	: Entity(location, velocity, setLaunchTexture, setFrames, textureLocation, setHeight, setWidth)
{
	launched = false;
	bounceNoise = setSound;
	gravity = 9.8f;
	angle  = 45;
	power = 0.0f;
	dx = 0.0f;
	dy = 0.0f;
	baseSprite->SetHotSpot(32,32);
	scaleX = (float)cos(angle * M_PI / 180.0);
	scaleY = (float)sin(angle * M_PI / 180.0);
	bounced = true;
	completed = false;
	flying = FlyingState();
	gliding = GlidingState();
	walking = WalkingState(setWalkTexture);
	launching = LaunchingState(setLaunchTexture);
	distance = 0;
	observerList = list<Observer*>();
	GoalManager* goalObserver = new GoalManager();
	observerList.push_back(goalObserver);
	observerList.push_back(inventoryObserver);
	baseSprite->SetHotSpot(64,64);
	state = &launching;
}

///<summary>
/// Default Constructor.
///</summary>
Gorgonal::Gorgonal()
{
	launched = false;
	gravity = 9.8f;
	power = 0.0f;
	angle = 45;
	dx = 0.0f;
	dy = 0.0f;
	bounced = true;
	completed = false;
	flying = FlyingState();
	gliding = GlidingState();
	walking = WalkingState();
	launching = LaunchingState();
	distance = 0;
	observerList = list<Observer*>();
	GoalManager* goalObserver = new GoalManager();
	observerList.push_back(goalObserver);
	state = &launching;
}

Gorgonal::~Gorgonal(void)
{
}

void Gorgonal::boom() 
{
	int pan = int((location.x-400)/4);
	float pitch = (dx*dx+dy*dy)*0.0005f+0.2f;
	hge->Effect_PlayEx(bounceNoise,100,pan,pitch);
}

///<summary>
/// In this method we set the initial parameters for launch.
///</summary>
void Gorgonal::launch(float angle1, float power1)
{
	angle = angle1;
	power = power1 * 2;
	scaleX = (float)cos(angle * M_PI / 180.0);
	scaleY = (float)sin(angle * M_PI / 180.0);
	velocity.x = power * scaleX;
	velocity.y = power * scaleY;
	completed = false;
	launched = true;
}

void Gorgonal::draw()
{
	state->draw(*this);
}

///<summary>
/// In this class our update delegates to the state the player is currently in.
///</summary>
void Gorgonal::update(float angleIn)
{	
	angle = angleIn;
	state->update(*this);
}

///<summary>
/// In this method we reset the class back to its default conditions.
///</summary>
void Gorgonal::reset()
{
	state = &launching;
	launched = false;
	completed = false;
	location.x = startLocation.x;
	location.y = startLocation.y;
	velocity.x = 0;
	velocity.y = 0;
	dy = 0;
	power = 0;
	angle = 0;
	dx = 0;
}