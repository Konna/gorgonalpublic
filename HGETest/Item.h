#pragma once

#ifndef _ItemH
#define _ItemH

#include "Entity.h"
#include "HGE\include\hgerect.h"
#include "HGE\include\hgevector.h"
#include "Inventory.h"

using namespace std;

class Item : public Entity
{
public:
	enum type {coin, part};
	Item(hgeVector velocity, hgeVector location, Inventory* inventoryObserver, HTEXTURE &setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth, type setType);
	Item();
	~Item(void);
	int getCoinCount() {return coinCount;};
	int getPartCount() {return partCount;};
	type getType() { return itemType;};
	virtual void draw();
	void collision();
	bool getColliding(){return colliding;};
private:
	void setTextureLocation();
	int coinCount;
	int partCount;
	hgeVector texLocation;
	string consolePrefix;
	type itemType;
};
#endif

