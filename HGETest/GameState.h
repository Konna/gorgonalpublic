#ifndef _GameStateH
#define _GameStateH

#pragma once
#include "WrapperState.h"
#include "DayState.h"
#include "ShopState.h"
#include "MenuState.h"

class GameState
{
public:

	static DayState day;
	static MenuState menu;
	static ShopState shop;

	void setState(WrapperState* state); 
	void resetGame();
	WrapperState* getState(){return currentState;};
	GameState(HTEXTURE* startMenuTex, HTEXTURE* shopMenuTex, Wrapper* wrapper);
	~GameState(void);

private:

	WrapperState* currentState;
	WrapperState* previousState;
};
#endif 
