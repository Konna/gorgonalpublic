#include "LogicRectangle.h"


///<summary>
/// Main Constructor.
///</summary>
LogicRectangle::LogicRectangle(float setCentreX, float setCentreY, float setWidth, float setHeight)
{
	centreX = setCentreX;
	centreY = setCentreY;
	width = setWidth;
	height = setHeight;
}

///<summary>
/// Default Constructor.
///</summary>
LogicRectangle::LogicRectangle()
{
}

LogicRectangle::~LogicRectangle(void)
{
}

///<summary>
/// Gives the rectangle as a haaf rectangle for use with collision.
///</summary>
hgeRect LogicRectangle::asHGERect()
{
	hgeRect result;
	result.x1 = (centreX - (width / 2));
	result.y1 = (centreY - (height / 2));
	result.x2 = (centreX + (width / 2));
	result.y2 = (centreY + (height / 2));
	return result;
}
