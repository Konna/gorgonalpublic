#include "Obstacle.h"

///<summary>
/// Base class for the pick up items in the game.
///</summary>
Obstacle::Obstacle(hgeVector velocity, hgeVector location, HTEXTURE &setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth, type setType) 
	: Entity(location, velocity, setTexture, setFrames, textureLocation, setHeight, setWidth)
{
	obstacleType = setType;
	consolePrefix = "[Obstacle] " + obstacleType;
	baseSprite->SetHotSpot(0,64);
	setTextureLocation();
	angle = 0;
}

Obstacle::Obstacle()
{
	obstacleType = type::bouncy;
	consolePrefix = "[Obstacle] Bouncy";
}

Obstacle::~Obstacle(void)
{
	std::cout << consolePrefix << "cleaning obstacle" << std::endl;
}

///<summary>
/// This method should set the texture to the appropriate one from the sprite sheet.
///</summary>
void Obstacle::setTextureLocation()
{
	switch(obstacleType)
	{
	case bouncy:
		{
			baseSprite->SetTextureRect(0,0,64,64);
		}
		break;
	case slow:
		{
			baseSprite->SetTextureRect(0,0,64,64);
		}
		break;
	case spike:
		{
			baseSprite->SetTextureRect(0,0,64,64);
		}
		break;
	}
}

///<summary>
/// Draws the objective.
///</summary>
void Obstacle::draw()
{
	baseSprite->Render(location.x,location.y);
}

float Obstacle::collision(float power)
{
	if(colliding)
	{
		switch(obstacleType)
		{
		case bouncy:
			{
				power = power * 3;
				return power;

			}
			break;
		case slow:
			{
				power = power / 1.5;
				return power;
			}
			break;
		case spike:
			{
				power = 0;
				return 0;
			}
			break;
		}
	}
}