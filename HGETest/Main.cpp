#include <Windows.h>
#include "HGE\include\hge.h"
#include "HGE\include\hgeresource.h"
#include "HGE\include\hgevector.h"
#include <string.h>
#include "Wrapper.h"
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>


static std::string consolePrefix = "[Main]"; 
Wrapper gameWrapper = Wrapper();
HGE *hge = 0;
hgeResourceManager *res = 0;

bool frameFunc()
{
	float dt=hge->Timer_GetDelta();
	if (hge->Input_GetKeyState(HGEK_ESCAPE)) return true;	
	gameWrapper.update(dt);
	return false;
}

bool renderFunc()
{
	hge->Gfx_BeginScene();
	hge->Gfx_Clear(0);
	gameWrapper.draw();
	hge->Gfx_EndScene();
	return false;
}

void loadTextures()
{
	std::list<string>::iterator itr;
	std::cout << consolePrefix << " Loading Textures..." << std::endl;
	for(itr = gameWrapper.textureFilePath.begin(); itr != gameWrapper.textureFilePath.end(); ++itr)
	{
		std::cout << consolePrefix << "	Loading: " << (*itr) << std::endl;
		gameWrapper.textures.push_back(hge->Texture_Load((*itr).c_str()));
		if(res->GetTexture((*itr).c_str()) == 0)
		{
			HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
			std::cout << consolePrefix << " Failed to load: " << (*itr) << std::endl;
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		}
	}
	std::cout << consolePrefix << " Textures Loaded." << std::endl;
}

void loadSounds()
{
	std::list<string>::iterator itr;
	std::cout << consolePrefix << " Loading Sounds..." << std::endl;
	for(itr = gameWrapper.audioFilePath.begin(); itr != gameWrapper.audioFilePath.end(); ++itr)
	{
		std::cout << consolePrefix << "	Loading: " << (*itr) << std::endl;
		gameWrapper.sounds.push_back(hge->Effect_Load((*itr).c_str()));
		if(res->GetEffect((*itr).c_str()) == 0)
		{
			HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
			std::cout << consolePrefix << " Failed to load: " << (*itr) << std::endl;
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		}
	}
	std::cout << consolePrefix << " Sounds Loaded." << std::endl;
}

void loadMusic()
{
	std::list<string>::iterator itr;
	std::cout << consolePrefix << " Loading Music..." << std::endl;
	for(itr = gameWrapper.musicFilePath.begin(); itr != gameWrapper.musicFilePath.end(); ++itr)
	{
		std::cout << consolePrefix << " Loading: " << (*itr) << std::endl;
		gameWrapper.music.push_back(hge->Music_Load((*itr).c_str()));
		if(res->GetMusic((*itr).c_str()) == 0)
		{
			HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
			std::cout << consolePrefix << " Failed to load: " << (*itr) << std::endl;
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		}
	}
	std::cout << consolePrefix << " Music Loaded." << std::endl;
}

void unloadTextures()
{
	std::list<HTEXTURE>::iterator itr;
	for(itr = gameWrapper.textures.begin(); itr != gameWrapper.textures.end(); ++itr)
	{
		hge->Texture_Free((*itr));
	}
	gameWrapper.textures.clear();
}

void unloadSounds()
{
	std::list<HEFFECT>::iterator itr;
	for (itr = gameWrapper.sounds.begin(); itr != gameWrapper.sounds.end(); ++itr)
	{
		hge->Effect_Free((*itr));
	}
	gameWrapper.sounds.clear();
}

void unloadMusic()
{
	std::list<HMUSIC>::iterator itr;
	for (itr = gameWrapper.music.begin(); itr != gameWrapper.music.end(); ++itr)
	{
		hge->Music_Free((*itr));
	}
	gameWrapper.music.clear();
}

void consoleStuff()
{

	int hConHandle;

	long lStdHandle;

	CONSOLE_SCREEN_BUFFER_INFO coninfo;

	FILE *fp;

	// allocate a console for this app

	AllocConsole();

	// set the screen buffer to be big enough to let us scroll text
	SetConsoleTitle("Dev Console");
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),

		&coninfo);


	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),

		coninfo.dwSize);

	// redirect unbuffered STDOUT to the console

	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stdout = *fp;

	setvbuf( stdout, NULL, _IONBF, 0 );

	// redirect unbuffered STDIN to the console

	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "r" );

	*stdin = *fp;

	setvbuf( stdin, NULL, _IONBF, 0 );

	// redirect unbuffered STDERR to the console

	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stderr = *fp;

	setvbuf( stderr, NULL, _IONBF, 0 );

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog

	// point to console as well

	std::ios::sync_with_stdio();

}
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	consoleStuff();
	// Get HGE interface
	std::cout << consolePrefix << " Creating haaf engine version: " << HGE_VERSION << std::endl;
	hge = hgeCreate(HGE_VERSION);
	std::cout << consolePrefix << " Created haaf engine version: " << HGE_VERSION << std::endl;
	// Set up log file, frame function, render function and window title
	hge->System_SetState(HGE_LOGFILE, "hge_tut02.log");
	hge->System_SetState(HGE_FRAMEFUNC, frameFunc);
	hge->System_SetState(HGE_RENDERFUNC, renderFunc);
	hge->System_SetState(HGE_TITLE, "HGE Tutorial 02 - Using input, sound and rendering");
	hge->System_SetState(HGE_USESOUND, false);

	// Set up video mode

	hge->System_SetState(HGE_WINDOWED, true);
	hge->System_SetState(HGE_SCREENWIDTH, 800);
	hge->System_SetState(HGE_SCREENHEIGHT, 600);
	hge->System_SetState(HGE_SCREENBPP, 32);
	hge->System_SetState(HGE_SHOWSPLASH, false);
	std::cout << consolePrefix << " Attempting to Initiate Haaf system" << std::endl;
	if(hge->System_Initiate())
	{
		res = new hgeResourceManager();
		std::cout << consolePrefix << " The system has Initiated" << std::endl;
		// Load sound and texture
		gameWrapper.initAssets();
		loadTextures();
		loadSounds();
		loadMusic();
		gameWrapper.initialise();

		hge->System_Start();

		// Free loaded texture and sound
		unloadTextures();
		unloadSounds();
		unloadMusic();
	}
	else MessageBox(NULL, hge->System_GetErrorMessage(), "Error", MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);
	// Clean up and shutdown
	delete res;
	hge->System_Shutdown();
	hge->Release();
	return 0;
}



