#pragma once

#ifndef _BaseMenuH
#define _BaseMenuH

#include <list>
#include <io.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include "HGE\include\hgegui.h"
#include "HGE\include\hgeguictrls.h"
#include "Observer.h"

using namespace std;

class WrapperState;
class Wrapper;
class BaseMenu
{
public:
	hgeGUI* menu;
	BaseMenu(void);
	~BaseMenu(void);
	virtual void draw();
	virtual void update(float delta);
	hgeSprite* background;
	hgeFont* menuFont;	
	float height;
	float width;
protected:
	Wrapper* wrapper;
};
#endif

