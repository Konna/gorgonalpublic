#pragma once
#include "erastate.h"
class MedievalEra :
	public EraState
{
public:
	MedievalEra(void);
	~MedievalEra(void);
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos);
};

