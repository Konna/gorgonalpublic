#pragma once
#ifndef _PlayerStateH
#define _PlayerStateH
#include "Entity.h"
class Gorgonal;
class PlayerState
{
public:
	PlayerState(void);
	virtual ~PlayerState(void);
	virtual void enter(Gorgonal& player);
	virtual void draw(Gorgonal& player);
	virtual void update(Gorgonal& player);
	string name() { return stateName;};

protected:
	bool startedInState;
	string stateName;
};
#endif
