#pragma once
#ifndef _EraStateH
#define _EraStateH

#include "HGE\include\hge.h"
#include "HGE\include\hgevector.h"

class WorldHandling;
class EraState
{
public:
	EraState(void);
	virtual ~EraState(void);
	virtual void enter();
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos);
};
#endif

