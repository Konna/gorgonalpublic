#pragma once
#ifndef _GlidingStateH
#define _GlidingStateH
#include "flyingstate.h"
class GlidingState : public FlyingState
{
public:
	GlidingState(void);
	GlidingState(HTEXTURE setGlidingSheet);
	~GlidingState(void);
	void enter(Gorgonal& player);
	virtual void draw(Gorgonal& player);
	virtual void update(Gorgonal& player);

private:
	HTEXTURE glidingSheet;
};
#endif

