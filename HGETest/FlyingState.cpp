#include "FlyingState.h"
#include "Gorgonal.h"


///<summary>
/// Default Constructor.
///</summary>
FlyingState::FlyingState(void)
{
	gravity = 9.8f;
	startedInState = false;
	stateName = "FlyingState";
}


FlyingState::~FlyingState(void)
{
}

///<summary>
/// In this method we draw the correct sprite for player flying in the air
///</summary>
void FlyingState::draw(Gorgonal& player)
{
	float drawAngle = player.angle - 90;
	player.baseSprite->RenderEx(player.location.x,player.location.y,-(drawAngle * (M_PI/180)));
}

///<summary>
/// In this method we update the player while it in flying in the air
///</summary>
void FlyingState::update(Gorgonal& player)
{
	float delta = (float)player.hge->Timer_GetDelta();
	//if (player->hge->Input_GetKeyState(HGEK_SPACE))
	//{
	//	player->state = &player->gliding;
	//}
	if (!player.completed)
	{
		if (player.power > 500) player.power = 500;
 		player.velocity.y -= (gravity * (5 * delta));
		if((player.location.y > 0)||(player.bounced))
 		{
 			player.power = player.power * 0.75;
 			if(player.power < 1)
 			{
 				player.power = 0;
 			}
 			player.velocity.y = (player.power) * player.scaleY;
 			player.velocity.x = (player.power) * player.scaleX;
			player.bounced = false;
 		}
		player.location.y += (-player.velocity.y * (5 * delta));
		player.location.x += (player.velocity.x * (5 * delta));
		if(player.location.y <= -3300) player.location.y = -3300;
		if((player.power == 0) && (player.location.y >= 0))
		{
			player.distance = player.location.x;
			player.completed = true;
			player.notify(player,Observer::MAIN_GOAL);
			player.notify(player,player.distance);
			std::cout << player.consolePrefix << "Final distance: " << player.getDistance() << std::endl;
			player.state = &player.walking;
		}
	}
}