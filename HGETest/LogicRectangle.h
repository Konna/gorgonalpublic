#pragma once
#ifndef _LogicRectH
#define _LogicRectH

#include "HGE\include\hgerect.h"

class LogicRectangle
{
public:
	LogicRectangle(float setCentreX, float setCentreY, float setWidth, float setHeight);
	LogicRectangle();
	~LogicRectangle(void);
	hgeRect asHGERect();
	float centreX, centreY;
	float width, height;
};
#endif

