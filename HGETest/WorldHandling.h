#pragma once

#ifndef _WorldH
#define _WorldH

#include <math.h>
#include <list>


#include "HGE\include\hge.h"
#include "HGE\include\hgevector.h"

#include "Item.h"
#include "Obstacle.h"
#include "Camera.h"
#include "LogicRectangle.h"
#include "EraState.h"
#include "AncientEra.h"
#include "FutureEra.h"
#include "IndustrialEra.h"
#include "MedievalEra.h"
#include "MordernEra.h"
#include "RenaissanceEra.h"


using namespace std;
class WorldHandling
{
	friend class AncientEra;
	friend class EraState;
	friend class FutureEra;
	friend class MordernEra;
	friend class MedievalEra;
	friend class RenaissanceEra;
	friend class IndustrialEra;

public:
	static AncientEra ancientEra;
	static FutureEra futureEra;
	static MordernEra mordernEra;
	static MedievalEra medievalEra;
	static RenaissanceEra renaissanceEra;
	static IndustrialEra industrialEra;

	WorldHandling(HTEXTURE* setItemTexture,HTEXTURE* setObstacleTexture, hgeVector playerPos, Inventory* inventory);
	~WorldHandling(void);
	void update(float delta, hgeVector newLocation);
	void draw();
	void reset();
	void setState();
	LogicRectangle itemGenBorder;
	LogicRectangle scenerayGenBorder;
	LogicRectangle enemyGenBorder;
	list<Item> itemList;
	list<Obstacle> obstacleList;
private:
	void cleanUpItems(hgeVector playerPos);
	void generateNewItems(HTEXTURE* itemText, hgeVector playerPos);
	void generateNewObstacles(hgeVector playerPos);
	HTEXTURE itemTexture[2];
	HTEXTURE obstacleTexture[18];
	Inventory* inventoryObserver;
	int itemLimit;
	int obstacleLimit;
	bool checkBorders(hgeRect a, hgeRect b);
	hgeVector itemLocation;
	hgeVector obstacleLocation;
	EraState* state;
};
#endif

