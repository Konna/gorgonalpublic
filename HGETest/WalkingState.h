#pragma once
#ifndef _WalkingStateH
#define _WalkingStateH
#include "playerstate.h"
class WalkingState : public PlayerState
{
public:
	WalkingState(void);
	WalkingState(HTEXTURE &setWalkingSheet);
	~WalkingState(void);
	void enter(Gorgonal& player);
	void exit();
	virtual void draw(Gorgonal& player);
	virtual void update(Gorgonal& player);

private:
	HTEXTURE walkingSheet;
	int frame;
	float lastFrameTime;
	float frameTime;
	float stateTime;
	float startTime;
};
#endif

