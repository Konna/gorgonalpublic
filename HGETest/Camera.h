#pragma once

#ifndef _CameraH
#define _CameraH

#include "HGE\include\hge.h"
#include "HGE\include\hgevector.h"
#include "HGE\include\hgerect.h"
#include "LogicRectangle.h"
#include <stdio.h>
#include <io.h>
#include <iostream>

using namespace std;
class PlayerState;
class Camera
{
public:
	
	Camera(void);
	~Camera(void);
	void draw(PlayerState* state);
	void reset();
	void update(float delta);
	void setX(float value){ dx = value;};
	void setY(float value){ dy = value;};
	bool wKeyFlag, sKeyFlag, aKeyFlag, dKeyFlag;
	LogicRectangle viewPort;

private:

	HGE* hge;
	float dx, dy;
	float speed;

};
#endif

