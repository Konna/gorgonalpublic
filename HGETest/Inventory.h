#pragma once

#ifndef _InventoryH
#define _InventoryH

#include <stdio.h>
#include <io.h>
#include <string>
#include <string.h>
#include <list>
#include <algorithm>

#include "Observer.h"

using namespace std;
class Inventory : public Observer
{
public:
	Inventory(void);
	~Inventory(void);
	int getCoins() { return coins;};
	bool contains(string checkUpgrade);
	void update();
	virtual void onNotify(Entity& entity, int distance);
	virtual void onNotify(Entity& entity, Observer::Hit hit);
	virtual void onNotify(Entity& entity, Observer::Purchase purchase);
	void setCoins(int setCoin);
	void notify(Inventory& inventory, int currentCoins);
	bool checkUnlocked(Observer::Purchase checkThis);
protected:
	list<Observer*> observerList;
	list<Observer::Purchase> unlockedList;
	list<Observer::Purchase> lockedList;
	list<Observer::Purchase> purchasedList;
private: // maybe protected for this but it shouldn't make much difference since this shouldn't be inherited from
	list<string> upgradeList;
	int coins;
	int distanceToCoin;
	int fuel;
};
#endif

