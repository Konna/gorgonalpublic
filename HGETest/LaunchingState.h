#pragma once
#ifndef _LaunchingStateH
#define _LaunchingStateH
#include "playerstate.h"
class LaunchingState : public PlayerState
{
public:
	LaunchingState(void);
	LaunchingState(HTEXTURE &setLaunchingSheet);
	~LaunchingState(void);
	void enter(Gorgonal& player);
	virtual void draw(Gorgonal& player);
	virtual void update(Gorgonal& player);

private:
	HTEXTURE launchingSheet;
};
#endif

