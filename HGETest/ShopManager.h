#pragma once
#ifndef _ShopManagerH
#define _ShopManagerH
#include "observer.h"
#include "Inventory.h"
#include "HGE\include\hgeguictrls.h"
class BaseMenu;
class ShopManager : public Observer
{
public:
	ShopManager(void);
	~ShopManager(void);
	void onNotify(Inventory& inventory, int currentCoins);
	void notify(Entity& entity, Observer::Purchase purchase);
	void onNotify(BaseMenu& menu, hgeGUIButton button);
	int getCoins(){return availableCoins;};
	int getEraPrice(){return eraPrice;};
	int purchase(int price, int coins);
	void setEraPrice();
	void update();
private:
	int availableCoins;
	int eraPrice;
};
#endif
