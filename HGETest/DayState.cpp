#include "DayState.h"
#include "Wrapper.h"


///<summary>
/// Default Constructor.
///</summary>
DayState::DayState(void)
{
	previousState = &Gorgonal::launching;
	currentState = &Gorgonal::launching;
}


DayState::~DayState(void)
{
}


///<summary>
/// In this method we update the game while the launch events are happening.
///</summary>
void DayState::update(Wrapper& wrapper)
{
	previousState = currentState;
	currentState = wrapper.testBall->state;
	if(wrapper.hge->Input_GetKeyState(HGEK_TAB))
	{
		wrapper.state = &GameState::shop;
	}
	if (currentState != previousState && previousState->name() == "WalkingState") wrapper.state = &GameState::shop;
	if (currentState != previousState && currentState->name() != "WalkingState")
	{
		wrapper.camera->reset();
		wrapper.worldControl->reset();
		wrapper.testBackground->reset();
	}
	float delta = wrapper.hge->Timer_GetDelta();
	if (wrapper.testBall->state->name() != "LaunchState")
	{
		wrapper.camera->setX((800/2)-wrapper.testBall->getLocation().x);
		wrapper.camera->setY((600/2)-wrapper.testBall->getLocation().y);
	}
	if (wrapper.hge->Input_GetKeyState(HGEK_UP))
	{
		if (wrapper.testBall->state->name() == "LaunchState")
			wrapper.testLauncher->upKeyFlag = true;
	}
	else 
	{
		wrapper.testLauncher->upKeyFlag = false;
	}
	if (wrapper.hge->Input_GetKeyState(HGEK_DOWN))
	{
		if (wrapper.testBall->state->name() == "LaunchState")
			wrapper.testLauncher->downKeyFlag = true;
	}
	else 
	{

		wrapper.testLauncher->downKeyFlag = false;
	}
	if (wrapper.hge->Input_GetKeyState(HGEK_LEFT))
	{
		if (wrapper.testBall->state->name() == "LaunchState")
			wrapper.testLauncher->leftKeyFlag = true;
	}
	else 
	{
		wrapper.testLauncher->leftKeyFlag = false;
	}
	if (wrapper.hge->Input_GetKeyState(HGEK_RIGHT))
	{
		if (wrapper.testBall->state->name() == "LaunchState")
		{
			wrapper.testLauncher->rightKeyFlag = true;
		}
	}
	else 
	{

		wrapper.testLauncher->rightKeyFlag = false;
	}
	if (wrapper.hge->Input_GetKeyState(HGEK_SPACE))
	{
		wrapper.testLauncher->spaceKeyFlag = true;
		if (wrapper.testBall->state->name() == "LaunchState")
		{
			wrapper.testBall->launch(wrapper.testLauncher->getAngle(),wrapper.testLauncher->getPower());
		}
	}
	else
	{
		wrapper.testLauncher->spaceKeyFlag = false;
	}
	list<Obstacle>::iterator itr;
	for(itr = wrapper.worldControl->obstacleList.begin();itr != wrapper.worldControl->obstacleList.end();itr++)
	{
		if(itr->getColliding() == false)
		{
			if(itr->checkCollision(wrapper.testBall))
			{
				float power = itr->collision(wrapper.testBall->getPower());
				wrapper.testBall->setPower(power);
				wrapper.testBall->bounced = true;
			}
		}
	}
	list<Item>::iterator itr2;
	for(itr2 = wrapper.worldControl->itemList.begin();itr2 != wrapper.worldControl->itemList.end();itr2 ++)
	{
		if(itr2->getColliding() == false)
		{
			if(itr2->checkCollision(wrapper.testBall))
			{
				itr2->collision();
			}
		}
	}
	wrapper.testBall->update(wrapper.testLauncher->getAngle());
	wrapper.testLauncher->update(delta);
	wrapper.testBackground->update(wrapper);
	wrapper.worldControl->update(delta, wrapper.testBall->getLocation());
	wrapper.camera->update(delta);
}


///<summary>
/// In this method we draw the relevent objects while the launch events are happening.
///</summary>
void DayState::draw(Wrapper& wrapper)
{
	wrapper.testBackground->draw();
	wrapper.testBall->draw();
	wrapper.testLauncher->draw();

	wrapper.worldControl->draw();
	wrapper.camera->draw(wrapper.testBall->state);
}