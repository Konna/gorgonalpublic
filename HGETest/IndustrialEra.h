#pragma once
#include "erastate.h"
class IndustrialEra :
	public EraState
{
public:
	IndustrialEra(void);
	~IndustrialEra(void);
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos);
};

