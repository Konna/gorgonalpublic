#pragma once
#ifndef _GoalManagerH
#define _GoalManagerH

#include <list>
#include "Observer.h"
using namespace std;

class GoalManager : public Observer
{
public:
	GoalManager(void);
	~GoalManager(void);
	void update();
	virtual void onNotify(Entity& entity, Observer::Goal goal);
};
#endif

