#include "Purchase.h"


///<summary>
/// Default Constructor.
///</summary>
Purchase::Purchase(void)
{
	type = "";
	cost = 0;
}

///<summary>
/// Main Constructor.
///</summary>
Purchase::Purchase(string setType, int setCost)
{
	type = setType;
	cost = setCost;
}

Purchase::~Purchase(void)
{
}
