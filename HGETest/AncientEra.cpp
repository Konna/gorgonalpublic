#include "AncientEra.h"
#include "WorldHandling.h"

AncientEra::AncientEra(void)
{
}


AncientEra::~AncientEra(void)
{
}

void AncientEra::generateNewObstacles(WorldHandling& world, hgeVector playerPos)
{
	world.obstacleLocation.x += (rand() % 400) + 400;
	world.obstacleLocation.y = 0;
	switch(rand() % 3)
	{
	case 0:
		{
			Obstacle bouncy = Obstacle(hgeVector(0,0),world.obstacleLocation,world.obstacleTexture[0],1,hgeVector(0,0),64.0f,64.0f,bouncy.bouncy);
			world.obstacleList.push_back(bouncy);
			break;
		}
	case 1:
		{
			Obstacle slow = Obstacle(hgeVector(0,0),world.obstacleLocation,world.obstacleTexture[6],1,hgeVector(0,0),64.0f,64.0f,slow.slow);
			world.obstacleList.push_back(slow);
			break;
		}
	case 2: 
		{
			Obstacle spike = Obstacle(hgeVector(0,0),world.obstacleLocation,world.obstacleTexture[12],1,hgeVector(0,0),64.0f,64.0f,spike.spike);
			world.obstacleList.push_back(spike);
			break;
		}
	}
}