#include "RenaissanceEra.h"
#include "WorldHandling.h"

RenaissanceEra::RenaissanceEra(void)
{
}


RenaissanceEra::~RenaissanceEra(void)
{
}

void RenaissanceEra::generateNewObstacles(WorldHandling& world, hgeVector playerPos)
{
	world.obstacleLocation.x += (rand() % 400) + 400;
	world.obstacleLocation.y = 0;
	switch(rand() % 3)
	{
	case 0:
		{
			Obstacle bouncy = Obstacle(hgeVector(0,0),world.obstacleLocation,world.obstacleTexture[2],1,hgeVector(0,0),64.0f,64.0f,bouncy.bouncy);
			world.obstacleList.push_back(bouncy);
			break;
		}
	case 1:
		{
			Obstacle slow = Obstacle(hgeVector(0,0),world.obstacleLocation,world.obstacleTexture[8],1,hgeVector(0,0),64.0f,64.0f,slow.slow);
			world.obstacleList.push_back(slow);
			break;
		}
	case 2: 
		{
			Obstacle spike = Obstacle(hgeVector(0,0),world.obstacleLocation,world.obstacleTexture[14],1,hgeVector(0,0),64.0f,64.0f,spike.spike);
			world.obstacleList.push_back(spike);
			break;
		}
	}
}