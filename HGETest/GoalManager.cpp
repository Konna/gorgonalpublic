#include "GoalManager.h"
#include "Entity.h"

///<summary>
/// Default Constructor.
///</summary>
GoalManager::GoalManager(void)
{
}

GoalManager::~GoalManager(void)
{
}

void GoalManager::update()
{
}

void GoalManager::onNotify(Entity& entity, Observer::Goal goal)
{
	switch(goal)
	{
	case MAIN_GOAL:
		{
			//Do something
			break;
		};
	default: 
		{
			return;
		};
	};
}