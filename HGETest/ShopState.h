#pragma once
#ifndef _ShopStateH
#define _ShopStateH
#include "wrapperstate.h"
#include "ShopMenu.h"
class ShopState :
	public WrapperState
{
public:
	static ShopMenu shopMenu;
	ShopState(void);
	ShopState(HTEXTURE* setTexture, Wrapper* setWrapper);
	virtual ~ShopState(void);
	virtual void draw(Wrapper& wrapper);
	virtual void update(Wrapper& wrapper);
	virtual string name() { return "ShopState";};
};
#endif

