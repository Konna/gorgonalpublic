#include "StartMenu.h"
#include "GameState.h"
#include "Wrapper.h"
///<summary>
/// Deafult Constructor
///</summary>
StartMenu::StartMenu(void) : BaseMenu()
{

}


///<summary>
/// Constructor that takes an array of textures and sets up the controls.
///</summary>
StartMenu::StartMenu(HTEXTURE* setTextures, Wrapper* setWrapper) : BaseMenu()
{

	//TODO: These are temporaray to avoid compile errors they need actual values from image files.
	wrapper = setWrapper;
	int x = 0, y = 0, w = 800, h = 600, tx = 0, ty = 0;
	menu->SetCursor(new hgeSprite(setTextures[2],0,0,32,32));
	background = new hgeSprite(setTextures[0],x,y,w,h);
	menuFont = new hgeFont("menuFont.fnt");
	start = new hgeGUIButton(1,325,300,175,50,setTextures[1],0,0);
	credits = new hgeGUIButton(2,290,400,243,50,setTextures[1],0,50);
	exit = new hgeGUIButton(3,340,500,143,50,setTextures[1],0,100);
	menu->AddCtrl(start);
	menu->AddCtrl(credits);
	menu->AddCtrl(exit);
	menu->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
	menu->SetFocus(1);
	menu->Enter();
}

StartMenu::~StartMenu(void)
{
}

///<summary>
/// In this method we update our buttons based on user input.
///</summary>
void StartMenu::update(float delta)
{
	menu->Update(delta);
	if(start->GetState())
	{
		start->SetState(false);
		wrapper->onNotify(&GameState::day);
		std::cout << consolePrefix << " Start button pressed " << std::endl;
	}
	else if(credits->GetState())
	{
		credits->SetState(false);
		std::cout << consolePrefix << " ---------CREDITS--------- " << std::endl;
		std::cout << consolePrefix << " Lead Programmer: Conor Wood " << std::endl;
		std::cout << consolePrefix << " Support Programmer: Chris Boucher " << std::endl;
		std::cout << consolePrefix << " Art and Graphics: Luke Harris " << std::endl;
		std::cout << consolePrefix << " Testing and documentation: Seraf Kilinc " << std::endl;
		std::cout << consolePrefix << " -------------------------- " << std::endl;
	}
	else if(exit->GetState())
	{
		exit->SetState(false);
		std::cout << consolePrefix << " Exit button pressed " << std::endl;
		std::exit(1);
	}
}

///<summary>
/// In this method we override the base draw function in order to draw the background.
///</summary>
void StartMenu::draw()
{
	background->Render(0,0);
	BaseMenu::draw();
}