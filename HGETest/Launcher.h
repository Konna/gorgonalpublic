#pragma once

#ifndef _LauncherH
#define _LauncherH

#include "Entity.h"

using namespace std;
class Launcher : public Entity
{

public:
	Launcher(hgeVector velocity, hgeVector location, HTEXTURE* setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth);
	~Launcher(void);
	bool upKeyFlag, downKeyFlag, leftKeyFlag, rightKeyFlag, spaceKeyFlag;
	float getPower(){return power;};
	float getAngle(){return angle;};
	void reset();
	virtual void update(float delta);
	virtual void draw();

private:
	hgeSprite* launchPad;
	float power;
	float angle;
	bool increase;
	string consolePrefix;
};
#endif
