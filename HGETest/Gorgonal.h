#pragma once

#ifndef _GorgonalH
#define _GorgonalH

#define _USE_MATH_DEFINES
#include <math.h>

#include "Entity.h"
#include "FlyingState.h"
#include "GlidingState.h"
#include "LaunchingState.h"
#include "WalkingState.h"
#include "GoalManager.h"
#include "Inventory.h"
#include "WorldHandling.h"

class Gorgonal: public Entity
{
	friend class FlyingState; 
	friend class GlidingState;
	friend class LaunchingState;
	friend class WalkingState;
	friend class WorldHandling;
public:
	static FlyingState flying;
	static GlidingState gliding;
	static WalkingState walking;
	static LaunchingState launching;

	float getPower() { return power; };
	PlayerState* state;

	Gorgonal(hgeVector velocity, hgeVector location, Inventory* inventoryObserver, HEFFECT &setSound, HTEXTURE &setLaunchTexture, HTEXTURE &setFlightTexture, HTEXTURE &setWalkTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth);
	Gorgonal();
	~Gorgonal(void);

	void boom();
	virtual void draw();
	virtual void update(float delta);
	void setPower(float value){power = value;};
	float getDistance(){return distance;};
	void launch(float angle1, float power1);
	void reset();
	bool launched, completed;
	bool bounced;
	string consolePrefix;

private:
	float power, speed, friction, gravity, angle, distance;
	float t;
	float dx, dy;
	float scaleX, scaleY;
	HEFFECT bounceNoise;
};
#endif