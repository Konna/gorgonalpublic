#include "ShopManager.h"
///<summary>
/// Default Constructor.
///</summary>
ShopManager::ShopManager(void)
{
	eraPrice = 200;
}

ShopManager::~ShopManager(void)
{
}

///<summary>
/// this method is called from observered classes to allow the shop to keep up to date with current coins.
///</summary>
void ShopManager::onNotify(Inventory& inventory, int currentCoins)
{
	availableCoins = currentCoins;
}

///<summary>
/// In this method we update the shopManager to check current coins.
///</summary>
void ShopManager::update()
{

}

void ShopManager::setEraPrice()
{
	eraPrice = eraPrice * 2;
}

int ShopManager::purchase(int price,int coins)
{
	coins -= price;
	return coins;
}

///<summary>
/// This method is called from observered classes to allow the shop to manage purchases by button presses.
///</summary>
void ShopManager::onNotify(BaseMenu& menu, hgeGUIButton button)
{
	//Based on the button pressed call the notify for inventory
	//
	//if(Inventory::checkUnlocked(button.purchase))
	//{
	//	if( availableCoins < button.purchaseCost)
	//	{
	//		notify(
	//	}
	//}
}