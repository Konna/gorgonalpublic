#pragma once
#ifndef _DayStateH
#define _DayStatH
#include "wrapperstate.h"
#include "PlayerState.h"
class DayState :
	public WrapperState
{
public:
	DayState(void);
	~DayState(void);
	virtual void update(Wrapper& wrapper);
	virtual void draw(Wrapper& wrapper);
private:
	PlayerState* currentState;
	PlayerState* previousState;

};
#endif

