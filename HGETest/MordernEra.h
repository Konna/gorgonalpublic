#pragma once
#include "erastate.h"
class MordernEra :
	public EraState
{
public:
	MordernEra(void);
	~MordernEra(void);
	virtual void generateNewObstacles(WorldHandling& world, hgeVector playerPos);
};

