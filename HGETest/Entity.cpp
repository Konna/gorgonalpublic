#include "Entity.h"

HGE* Entity::hge = 0;

Entity::Entity(hgeVector setLocation, hgeVector setVelocity, HTEXTURE &setTexture, int setFrames, hgeVector &textureLocation, float setHeight, float setWidth)
{
	location = setLocation;
	startLocation = setLocation;
	velocity = setVelocity;
	height = setHeight;
	width = setWidth;
	frames = setFrames;
	angle = 45;
	if (hge == 0)
	{
		hge = hgeCreate(HGE_VERSION);
	}
	observerList = list<Observer*>();
	boundBox = hgeRect(location.x - (width / 2), location.y - (height /2), location.x + (width /2), location.y + (height / 2));	
 	baseSprite = new hgeAnimation(setTexture, frames, 4, textureLocation.x, textureLocation.y, width, height);
	baseSprite->GetBoundingBoxEx(location.x,location.y,angle* (M_PI/180),1,1,&boundBox);
 	baseSprite->Play();
	colliding = false;
}

///<summary>
/// Default constructor.
///</summary>
Entity::Entity()
{
	hge = hgeCreate(HGE_VERSION);
}

///<summary>
/// Need to delete the pointers on deconstruction of the object.
///</summary>
Entity::~Entity(void)
{
}

///<summary>
/// Basic collision detection can be improved upon.
///</summary>
bool Entity::checkCollision(Entity* object)
{
	if((location.x + width) < object->location.x)
	{
		colliding = false;
		return false;
	}
	if((location.x ) > (object->location.x + object->width))
	{
		colliding = false;
		return false;
	}
	if((location.y + height) < object->location.y)
	{
		colliding = false;
		return false;
	}
	if((location.y ) > (object->location.y + object->height))
	{
		colliding = false;
		return false;
	}
	colliding = true;
	return true;
}

///<summary>
/// Method to draw a basic colored rectangle using a quad.
///</summary>
void Entity::drawRect(int x, int y, int w, int h, int c,HTEXTURE tex)
{
	hgeQuad quad;
	quad.blend=BLEND_ALPHABLEND | BLEND_ZWRITE;

	quad.tex = tex;
	quad.v[0].x=x;	 quad.v[0].y=y;		quad.v[0].z = 0;	quad.v[0].col = c;	quad.v[0].tx = 0; quad.v[0].ty = 0;
	quad.v[1].x=x+w; quad.v[1].y=y;		quad.v[1].z = 0;	quad.v[1].col = c;	quad.v[1].tx = 1; quad.v[1].ty = 0;
	quad.v[2].x=x+w; quad.v[2].y=y+h;	quad.v[2].z = 0;	quad.v[2].col = c;	quad.v[2].tx = 1; quad.v[2].ty = 1;
	quad.v[3].x=x;	 quad.v[3].y=y+h;	quad.v[3].z = 0;	quad.v[3].col = c;	quad.v[3].tx = 0; quad.v[3].ty = 1;

	hge->Gfx_RenderQuad(&quad);			
}

int Entity::RGBATOCOLOR(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	return b | (g<<8) | (r<<16) | (a<<24);
}

int Entity::RGBTOCOLOR(char r, char g, char b)
{
	return RGBATOCOLOR(r,g,b,255);
}


void Entity::notify(Entity& entity, Observer::Goal goal)
{
	list<Observer*>::iterator itr;
	for(itr = observerList.begin(); itr != observerList.end(); ++itr)
	{
		(*itr)->onNotify(entity, goal);
	}
}


void Entity::notify(Entity& entity, int distance)
{
	list<Observer*>::iterator itr;
	for(itr = observerList.begin(); itr != observerList.end(); ++itr)
	{
		(*itr)->onNotify(entity, distance);
	}
}

void Entity::notify(Entity& entity, Observer::Hit hit)
{
	list<Observer*>::iterator itr;
	for (itr = observerList.begin(); itr != observerList.end(); ++itr)
	{
		(*itr)->onNotify(entity, hit);
	}
}


///<summary>
/// Virtual method incase later classes need to render more than just a base sprite.
///</summary>
void Entity::draw()
{
	baseSprite->Render(location.x,location.y);
}

///<summary>
/// Virtual method as almost all child classes will need to have some basic update logic.
///</summary>
void Entity::update(float delta)
{
	baseSprite->Update(delta);
	baseSprite->GetBoundingBoxEx(location.x,location.y,angle * (M_PI/180),1,1, &boundBox);
}